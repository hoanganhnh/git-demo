### GIT DEMO

`Shift+Ctrl+V` : preview README file

- **Tạo một kho lưu trữ - git init:**
  `git init `
- **Kiểm tra trạng thái - git status:**
  `git status`
- **Staging - git add:**

  > 1.  `git add <name file>` : thêm một file
  > 2.  `git add .` || `git add .` : thêm mọi thứ trong thư mục

- **Cam kết - git commit:**
  `git commit -m 'Message'` : di chuyển files từ Staging Area tới vùng Local Repository
- **Thêm remote repository vào cài đặt phía local**
- **Xem nội dung file git config:** `cat .git/config`
- **Tải lên máy chủ - git push:** `git push origin master`
- **Nhân bản kho lưu trữ - git clone:**
  1. Lấy source code từ remote về local: `git clone [path-to-repository]`
  2. Luốn thư mục clone về có một tên khác: `git clone [path-to-repository] new_folder_name`
- **Lấy các thay đổi từ máy chủ - git pull:**

  - Lấy các thay đổi từ nhánh “master” trên remote về nhánh “master” trên local: `git pull origin master`

- **Nhánh - branch:**

  _1. Tạo mới một nhánh - git branch:_ `git checkout -b new_branch_name`

  --> Trên working directory chỉ làm việc duy nhất với 1 nhánh

  _2. Chuyển nhánh - git checkout:_ ` git checkout branch_name`

  _3. Hợp nhất branch (B --> A) - git merge:_

  > 1. `git checkout master` : Di chuyển về nhánh `master`
  > 2. `git merge new_branch_name`
